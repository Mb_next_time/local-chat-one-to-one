.PHONY: all clientLinux clientWin32 clientWin64.exe clean

all: clientLinux clientWin.exe 
	
clientLinux: 
	g++ -std=c++11 -I$(PWD)/boost/include client.cpp -o clientLinux \
	$(PWD)/boost/lib/libboost_thread.a \
	$(PWD)/boost/lib/libboost_system.a -lpthread
		
clientWin.exe:
	x86_64-w64-mingw32-g++ -std=c++11 -I/usr/local/include client.cpp -o clientWin.exe \
	$(PWD)/boost/lib_mingw/lib/libboost_system-mt-x64.a \
	$(PWD)/boost/lib_mingw/lib/libboost_thread-mt-x64.a \
	/usr/x86_64-w64-mingw32/lib/libws2_32.a -static-libgcc -static-libstdc++
	
clean:
	rm -f clientLinux clientWin.exe 

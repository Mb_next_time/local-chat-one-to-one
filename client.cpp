#include <iostream>
#include <vector>
#include <algorithm>
#include <boost/thread.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/asio.hpp>
#include <boost/algorithm/string.hpp>

bool checkingIP(std::string &ip)
{	
	std::vector<std::string> tokens;
	boost::split(tokens,ip,boost::is_any_of("."));
	
	bool result = false;
	
	const int numberOctets = 4;
	
	if(tokens.size() != numberOctets)
	{	
		result = true;
		std::cout << "IP incorrect: " << ip << std::endl;
		return result;
	}
	
	std::find_if(tokens.begin(), tokens.end(), [&result](std::string &octet)
	{ 	
		try
		{	
			const int maxValueOctet = 255;
			const int minValueOctet = 0;
			
			if (boost::lexical_cast<int>(octet) > maxValueOctet || boost::lexical_cast<int>(octet) < minValueOctet)
			{
				result = true;
				return result;
			}
		}
		catch(const boost::bad_lexical_cast&)
		{
			result = true;
		}
	});
	
	if(result)
	{
		std::cout << "IP incorrect: " << ip << std::endl;
	}
	
	return result;
}

bool checkingPort(const std::string &portStr, unsigned short &port)
{
	try
	{	
		port = boost::lexical_cast<unsigned short>(portStr);
		
		const int maxValuePort = 65535;
		const int minValuePort = 0;
		
		if (port < maxValuePort && port > minValuePort)
		{
			return false;
		}
	}
	catch(const boost::bad_lexical_cast&)
	{	
		std::cout << "Port incorrect: " << portStr << std::endl;
		return true;
	}
}

boost::asio::ip::address getIPsource()
{
	boost::asio::io_service service;
	
	boost::asio::ip::udp::resolver resolver(service);
	boost::asio::ip::udp::resolver::query query(boost::asio::ip::udp::v4(), "google.com", "");
	boost::asio::ip::udp::resolver::iterator iter = resolver.resolve(query);
	boost::asio::ip::udp::endpoint ep = *iter;
	
	boost::asio::ip::udp::socket socket(service);
	socket.connect(ep);
	boost::asio::ip::address addr = socket.local_endpoint().address();
	socket.close();
	
	return addr;
}

void sender(const std::string &destIP, const int &portSend)
{	
	boost::asio::io_service service;
	boost::asio::ip::tcp::endpoint ep(boost::asio::ip::address::from_string(destIP), portSend);
	
	while(true)
	{	
		boost::asio::ip::tcp::socket sockSend(service);
		
		try
		{	
			sockSend.connect(ep);
			
			std::string msg;
			std::cout << "Send message" << std::endl;
			std::getline(std::cin, msg);
			
			sockSend.write_some(boost::asio::buffer(msg));
			sockSend.close();
		}
		catch(const boost::system::system_error&)
		{	
			std::cout << "Wait your talk partner" << std::endl;
			int timeOut = 5;
			boost::this_thread::sleep( boost::posix_time::seconds(timeOut));
			sockSend.close();
		}
	}
}

void receiver(const unsigned short &portRec)
{	
	boost::asio::io_service service;
	boost::asio::ip::address sourceIP  = getIPsource();
	
	boost::asio::ip::tcp::acceptor acceptor(service, boost::asio::ip::tcp::endpoint(sourceIP,portRec));
	
	while(true)
	{	
		boost::asio::ip::tcp::socket sockRec(service);
		acceptor.accept(sockRec);
		
		const int sizeBuf = 1024;
		char buf[sizeBuf];
		
		try
		{
			int bytes = sockRec.receive(boost::asio::buffer(buf, sizeBuf));
			
			if(bytes > 1)
			{	
				std::string msg(buf, bytes);
				std::cout << "Someone: " << msg << std::endl << std::endl;
			}
		}
		catch(const boost::system::system_error&)
		{
			
		}
		
		sockRec.close();
	}
}

int main(int argc, char* argv[])
{	
	try
	{	
		int correctCountArg = 4;
		
		if(argc != correctCountArg)
			throw argc;
	}
	catch(const int&)
	{
		std::cout << "Error with number arguments" << std::endl;
		std::cout << "1 arg: destantion IP" << std::endl;
		std::cout << "2 arg: port for sending message" << std::endl;
		std::cout << "3 arg: port for reciving message" << std::endl;

		return EXIT_FAILURE;
	}
	
	std::string destIP(argv[1]);
	
	if(checkingIP(destIP))
	{	
		return EXIT_FAILURE;
	}
	
	unsigned short portSend;
	unsigned short portRec;
	
	if(checkingPort(argv[2], portSend) || checkingPort(argv[3], portRec))
	{
		return EXIT_FAILURE;
	}
	
	boost::thread threadSender(sender, destIP, portSend);
	boost::thread threadReceiver(receiver, portRec);
	
	threadSender.join();
	threadReceiver.join();
	
	return EXIT_SUCCESS;
}

#!/bin/bash

#Версию boost можно изменить, поменяв переменные BOOST_VERSION_SF и BOOST_VERSION

BOOST_VERSION_SF=1.69.0;
BOOST_VERSION=1_69_0;

#скачиваем архив с boost
wget -O boost_${BOOST_VERSION}.tar.gz \
https://sourceforge.net/projects/boost/files/boost/${BOOST_VERSION_SF}/boost_${BOOST_VERSION}.tar.gz/download;

tar xzvf boost_${BOOST_VERSION}.tar.gz;	
rm -f boost_${BOOST_VERSION}.tar.gz;

boostDir=`pwd`;
cd boost_${BOOST_VERSION};

#устанавливаем boost
./bootstrap.sh --prefix=${boostDir};
./b2 --with-system --with-thread install;

#Собираем либы для x86_64-w64-mingw32-g++
echo "using gcc : : x86_64-w64-mingw32-g++ ;" > ${boostDir}/boost_${BOOST_VERSION}/tools/build/src/user-config.jam;

./b2 -a --layout=tagged --with-thread threading=multi target-os=windows \
variant=release link=static address-model=64 toolset=gcc --stagedir=${boostDir}/lib_mingw;
